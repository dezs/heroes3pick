﻿document.addEventListener( "DOMContentLoaded", onLoad, false );
var data = {
				'Замок':['Сорша','Кейтлин'],
				'Оплот':['Киррь','Ивор'],
				'Башня':['Солмир'],
				'Болото':['Андра'],
				'Крепость':['Десса','Гурнисон'],
				'Некрополис':['Исра'],
				'Темница':['??'],
				'Инферно':['??'],
				'Сопряжение':['??']}
var ui = new UI();
	
function onLoad(e){
    document.removeEventListener( "DOMContentLoaded", onLoad, false );
	ui.create(document.body, data);
	
}

function customMap(array, mapFunc){
	var result = [];
	for (var i=0;i<array.length;i++){
		result.push(mapFunc(array[i], i));
	}
	return result;
}

function UI(){
	self = this;
	

	function _$(selector){
		return document.querySelectorAll(selector);
	}
	
	function checkbox(){
		var input = document.createElement('input');
		input.type = 'checkbox';
		input.checked = true;
		return input;
	}
	
	function span(txt){
		var span = document.createElement('span');
		span.innerText = txt;
		return span;
	}
	
	function createLabelCheckbox(name, className){
		var label = document.createElement('label');
		if (typeof className == 'string')
			label.className = className;
		label.appendChild(checkbox());
		label.appendChild(span(name));
		return label;
	}
	
	function createP(name, heroes){
		var mainDiv = document.createElement('div');
		mainDiv.className = 'handler';
		mainDiv.appendChild(createLabelCheckbox(name));
		if (heroes.length > 0){
			var rightDiv = document.createElement('div');
			rightDiv.className = 'right';
			for (var i=0;i<heroes.length;i++){
				rightDiv.appendChild(createLabelCheckbox(heroes[i], 'hero'));
			}
			mainDiv.appendChild(rightDiv);		
			mainDiv.style.height = (heroes.length * 20)+'px';
		}
		return mainDiv;
	}
	
	var info;
	
	function createBtn(txt, clickEvent, className){
		var el = document.createElement('input');
		el.type = 'button';
		el.value = txt;
		el.className = className;
		el.addEventListener('click',clickEvent,false);
		return el;
	}
	
	function addInfo(txt){
		info.appendChild(span(txt));
	}
	
	function getRandom(){
		var inputs = _$('.right input');
		var range = [];
		for (var i=0;i<inputs.length;i++)
			if (inputs[i].checked)
				if (inputs[i].parentNode.parentNode.parentNode.firstChild.children[0].checked)
					range.push(inputs[i]);
		if (range.length == 0)
			 addInfo('Нет вариантов');
		else{
			var index = Math.random() * range.length | 0;
			range[index].checked = false;
			var castle = range[index].parentNode.parentNode.parentNode.firstChild.children[1].innerText;
			addInfo(castle + '  -  ' + range[index].parentNode.children[1].innerText);
		}
		//e.parentNode.parentNode.parentNode.firstChild.children[1].innerText - замок
		//e.parentNode.parentNode.parentNode.firstChild.children[0].checked - чекбокс
	}
	
	self.create = function(container, data){
		for (var i in data)
			container.appendChild(createP(i, data[i]));
		container.appendChild(createBtn('Рандом', getRandom,''));	
		info = document.createElement('div');
		info.className = 'info';
		container.appendChild(info);	
	}
}

function controller(){
	var data = [];
	
	this.connectCheckboxes = function(castle, hero){
		
	}
}

