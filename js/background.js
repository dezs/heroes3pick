﻿function init(launchData) {
  var options = {
    frame: 'chrome',
    minWidth: 400,
    minHeight: 400,
    width: 500,
    height: 600
  };

  chrome.app.window.create('index.html', options);
}

chrome.app.runtime.onLaunched.addListener(init);